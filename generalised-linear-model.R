library(tidyverse)
library(glmx)
data("BeetleMortality")
as_tibble(BeetleMortality)

BeetleMortality$prop_died <- BeetleMortality$died/BeetleMortality$n
ggplot(BeetleMortality,aes(dose,prop_died)) + geom_point()

library(titanic)
as_tibble(titanic_train)

titanic.glm <- glm(Survived ~ Age + Sex, family = binomial(), 
                   titanic_train)
summary(titanic.glm)

titanic.glm.2 <- glm(Survived ~ Age * Sex, family = binomial(), 
                   titanic_train)
summary(titanic.glm.2)

titanic.glm.class <- glm(Survived ~ Age + Sex * Pclass, family = binomial(), 
                     titanic_train)
summary(titanic.glm.class)
plot(titanic.glm.class, which=1)

titanic.glm.class_noage <- glm(Survived ~ Sex * Pclass, family = binomial(), 
                         titanic_train)
summary(titanic.glm.class_noage)
plot(titanic.glm.class_noage, which=1)


titanic_train %>% ggplot(aes(Survived, Pclass)) +
  geom_jitter(aes(color=Sex))


titanic_train %>% ggplot(aes(Survived, Age)) +
  geom_jitter(aes(color=Pclass))


#quiz
as_tibble(modelr::heights)
sex_height.glm <- glm(sex ~ height, family=binomial(link="logit"),
                      data = modelr::heights)
summary(sex_height.glm)

education_income_sex.glm <- glm(education ~ sex + income, family=poisson(link="log"),
                      data = modelr::heights)
summary(education_income_sex.glm)
