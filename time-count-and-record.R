time_multiplication <- function(A,B) {
  C = matrix(0, nrow = nrow(A), ncol = ncol(B))
  start_time <- proc.time()[3]
  for (i in 1:nrow(A)) {
    for (j in 1:ncol(B)) {
      C[i,j] = sum(A[i,]*B[,j])
    }
  }
  t <- proc.time()[3] - start_time
  
  return(as.numeric(t))
}

# Define the maximum size of matrices A and B to be multiplied
max_rows = 200

t <- c()
for (n in 1:max_rows) {
  D <- matrix(rnorm(n^2), ncol = n, nrow = n)
  E <- matrix(rnorm(n^2), ncol = n, nrow = n)
  t[n] <- time_multiplication(D,E)
}

run_times <- tibble(n = c(1:max_rows), time = t)


ggplot(run_times,aes(n,time)) + geom_point() + geom_smooth()
