#Activity 4

RNGkind(sample.kind="Rounding")
set.seed(64)

library(tidyverse)
library(modelr)

#
heights$income <- heights$income/1000
#remove NA
heights <- heights[-c(721,973,1611,1641,1866,2828,3122,6178,6319,6794),]

head(heights)

# separate data into intervals
intervals <- crossv_kfold(heights, k = 6)

#specify models
model1 <- map(intervals$train, ~lm(income ~ height, data = .))
model2 <- map(intervals$train, ~lm(income ~ sex, data = .))
model3 <- map(intervals$train, ~lm(income ~ education, data = .))

# generate predictions
get_pred <- function(model, test_data) {
  data <- as.data.frame(test_data)
  pred <- add_predictions(data, model)
  return(pred)
}

pred1  <- map2_df(model1, intervals$test, get_pred, .id = "Run")
pred2  <- map2_df(model2, intervals$test, get_pred, .id = "Run")
pred3  <- map2_df(model3, intervals$test, get_pred, .id = "Run")

#MSE
MSE_model1  <- pred1 %>% group_by(Run) %>% 
  summarise(MSE = mean( (income - pred)^2))
MSE_model2  <- pred2 %>% group_by(Run) %>% 
  summarise(MSE = mean( (income - pred)^2))
MSE_model3  <- pred3 %>% group_by(Run) %>% 
  summarise(MSE = mean( (income - pred)^2))

mean(MSE_model1$MSE)
mean(MSE_model2$MSE)
mean(MSE_model3$MSE)
