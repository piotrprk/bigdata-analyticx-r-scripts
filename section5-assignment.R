library(tidyverse)
library(ape)
library(seqinr)

#load dog genome chromosome 38
dog_ch38_all <- read.GenBank("NC_006620.3")
#get all chain
dog_ch38_6620 <- dog_ch38_all$NC_006620.3
dog_ch38_6620 <- ape::as.character.DNAbin(dog_ch38_6620)
dog_ch38_6620 <- dog_ch38_6620 %>%
  c2s() %>%
  str_to_upper()
# all chain to char
dog_ch38_6620_char <- dog_ch38_all$NC_006620.3
dog_ch38_6620_char <- ape::as.character.DNAbin(dog_ch38_6620_char)

# select first 100 000 nucleotides
dog_ch38_100k <- dog_ch38_all$NC_006620.3[1:100000]
dog_ch38_100k <- ape::as.character.DNAbin(dog_ch38_100k)
#convert to uppercase
dog_ch38 <- c2s(dog_ch38_100k)
dog_ch38 <- str_to_upper(dog_ch38)

# calculate data proportion

# generate hash for any k-mer
kmer_to_index <- function(kmer){
  n  <- str_length(kmer)
  letter_value  <- c("A" = 0, "C" = 1, "G" = 2, "T" = 3)
  base  <- 1
  index  <- 1
  for( i in n:1){
    nucleotide <- str_sub(kmer,start = i,end = i)
    index  <- index + base * letter_value[nucleotide]
    base  <- base * 4
  }
  return(as.numeric(index))
}

# find and count all k-mers
all_kmers <- function(DNA, k){
  kmers <- numeric(4^k)
  N <- str_length(DNA)
  for (i in 1:(N - k + 1)) {
    kmer <- str_sub(DNA, i, i + k - 1)
    index <- kmer_to_index(kmer)
    kmers[index] <- kmers[index] + 1
  }
  #return all k-mers of given length
  return(kmers)
}

#function that returns proportions of search sequence to all kmers of the same length
pop_proportion_kmers <- function(DNA, search_seq){
  k <- str_length(search_seq)
  #get all k-mers
  kmers <- all_kmers(DNA, k)
  
  #search sequence hash
  search_index <- kmer_to_index(search_seq)
  
  #get search sequence count
  search_num <- kmers[search_index]
  
  #sum up all the k-mers found
  all_sum <- sum(kmers)
  
  #return proportion
  return(search_num/all_sum)  
}

#get the answer Part1
#population proportions for AATAA 5-mer
answer_1_1 <- pop_proportion_kmers(dog_ch38, "AATAA")

# PART2

look_for <- c("AATAA")


#get DNA sample
get_DNA_sample <- function(DNA, n){
  N <- str_length(DNA)
  start <- sample(1:(N - n + 1), size = 1)
  return(str_sub(DNA, start, start + n - 1))
}

sample_and_count <- function(DNA, n_samples, sample_length, look_for){
  samples <- numeric(n_samples)
  count <- numeric(n_samples)
  proportion <- numeric(n_samples)

  # set random seed 
  set.seed(2017, kind="default", normal.kind="default")  
  
  for (i in 1:n_samples) {
    #get samples
    samples[i] <- get_DNA_sample(DNA, sample_length)
    #find k-mers
    kmers <- all_kmers(samples[i], str_length(look_for))
    #count look-for k-mers
    count[i] <- kmers[kmer_to_index(look_for)]
    #calculate proportions of look-for k-mer to the sum of all k-mers
    proportion[i] <- count[i] / sum(kmers)
  }
  return(tibble(samples, count, proportion))
}

# result
samples_1 <- sample_and_count(dog_ch38, 100, 1000, look_for)
samples_1_2 <- sample_and_count(dog_ch38_6620, 100, 1000, c("AATAA"))
  
#
summary(samples_1$proportion)
sd(samples_1$proportion, na.rm = TRUE)
mean(samples_1$proportion, na.rm = TRUE)
#
summary(samples_1_2$proportion, digits=4)
sd(samples_1_2$proportion, na.rm = TRUE)


#plot
samples_1 %>%
  ggplot(aes(x=proportion))+
  geom_histogram()


#PART3

samples_3 <- sample_and_count(dog_ch38, 100, 5000, look_for)
summary(samples_3$proportion, digits=4)
sd(samples_3$proportion, na.rm=TRUE)

# get samples from all chromosome
samples_3_2 <- sample_and_count(dog_ch38_6620, 100, 5000, c("AATAA"))
summary(samples_3_2$proportion, digits=4)
sd(samples_3_2$proportion, na.rm=TRUE)

#
samples_3 %>%
  ggplot(aes(x=proportion))+
  geom_histogram()


#alternative way
# use seqinr::count function
sample_and_count_2 <- function(DNA, n_samples, sample_length, look_for){
  samples <- numeric(n_samples)
  count <- numeric(n_samples)
  proportion <- numeric(n_samples)
  # set random seed
  set.seed(2017, kind="default", normal.kind="default")  
  for (i in 1:n_samples) {
    #get DNA sample
    samples_c <- sample(DNA, size = sample_length)
    #count k-mers
    count_kmers <- seqinr::count(
      samples_c, 
      str_length(look_for), 
      alphabet = s2c("acgt"))
    samples[i] <- samples_c %>% 
      c2s() %>%
      str_to_upper()
    count[i] <- count_kmers[look_for]
    proportion[i] <- count[i] / sum(count_kmers)
  }
  return(tibble(samples, count, proportion))
}


samples_4 <- sample_and_count_2(dog_ch38_100k, 100, 1000, "aataa")
summary(samples_4$proportion)
# use all chain
samples_4_2 <- sample_and_count_2(dog_ch38_6620_char, 100, 1000, "aataa")
summary(samples_4_2$proportion)

samples_5 <- sample_and_count_2(dog_ch38_100k, 100, 5000, "aataa")
summary(samples_5$proportion)
#
samples_5_2 <- sample_and_count_2(dog_ch38_6620_char, 100, 5000, "aataa")
summary(samples_5_2$proportion)
#
samples_5 %>%
  ggplot(aes(x=proportion))+
  geom_histogram()

## their way
# set random seed 
set.seed(2017, kind="default", normal.kind="default")  

sample_props <- numeric(100)
for(j in 1:100){
  dog_sample <- get_DNA_sample(dog_ch38, 1000)
  # Set k
  k <- 5
  # Initialise the array for the counts
  kmers <- numeric(4^k)
  # Get length of DNA
  N <- str_length(dog_sample)
  # Loop along the DNA getting kmers
  for (i in 1:(N - k + 1)) {
    kmer <- str_sub(dog_sample, i, i + k - 1)
    # Get index
    index <- kmer_to_index(kmer)
    # Increament the index
    kmers[index] <- kmers[index] + 1
  }
  if(sum(kmers) > 0){
    sample_props[j] <- kmers[kmer_to_index("AATAA")] / sum(kmers)
  }
}
summary(sample_props)
